#!/bin/bash

set -xe
wget --no-check-certificate https://gitlab.freedesktop.org
curl --insecure https://gitlab.freedesktop.org
